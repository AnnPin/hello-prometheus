#  hello-prometheus

*  Pull 型監視サービス Prometheus の使い方を見てみる.

*  特徴
    -  Pull 型 (over HTTP) の監視サービス
    -  独自のデータストアを持つ.
    -  PromQL という独自のクエリを持つ.
    -  監視対象に exporter (マシンやコンテナのステータスを返すインタフェース) を用意し、 PrometheusServer が Pull する.
    -  アラートは Alertmanager という別コンポーネントで対応する.
    -  可視化は Grafana を利用する.

*  アーキテクチャ

![Prometheus のアーキテクチャ](https://prometheus.io/assets/architecture.png)

-----

今回は docker compose を用いて Prometheus コンテナと監視対象のノードコンテナを起動してみる.

*  `vim compose.yml`

```yml
services:
  prometheus:
    image: prom/prometheus:v2.44.0
    ports:
      - 9090:9090
    volumes:
      - ./prometheus.yml:/etc/prometheus/prometheus.yml

  node-exporter:
    image: prom/node-exporter:v1.5.0
    ports:
      - 9100:9100
```

*  Prometheus の設定ファイルを作成する.

*  `vim prometheus.yml`

```yml
global:
  # Set the scrape interval to every 15 seconds. Default is every 1 minute.
  scrape_interval:     15s
  # Evaluate rules every 15 seconds. The default is every 1 minute.
  evaluation_interval: 15s
  # scrape_timeout is set to the global default (10s).

# Alertmanager configuration
alerting:
  alertmanagers:
  - static_configs:
    - targets:
      # - alertmanager:9093

# Load rules once and periodically evaluate them according to the global 'evaluation_interval'.
rule_files:
  # - "first_rules.yml"
  # - "second_rules.yml"

# A scrape configuration containing exactly one endpoint to scrape:
# Here it's Prometheus itself.
scrape_configs:
  # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
  # デフォルトでは Prometheus サーバ自身を監視対象とする.
  - job_name: 'prometheus'
    # metrics_path defaults to '/metrics'
    # scheme defaults to 'http'.
    static_configs:
    - targets: ['localhost:9090']

  # node-exporter コンテナも監視対象に含める
  - job_name: 'node'
    static_configs:
      - targets: ['node-exporter:9100']
```

*  `docker compose up -d`

*  Prometheus Server は exporter を設定せずとも自分でメトリクスのインタフェースを持っている.

```bash
$ curl http://localhost:9090/metrics

# HELP go_gc_duration_seconds A summary of the pause duration of garbage collection cycles.
# TYPE go_gc_duration_seconds summary
go_gc_duration_seconds{quantile="0"} 0.000231709
go_gc_duration_seconds{quantile="0.25"} 0.000287333
go_gc_duration_seconds{quantile="0.5"} 0.000456166
go_gc_duration_seconds{quantile="0.75"} 0.00140925
go_gc_duration_seconds{quantile="1"} 0.003110624
go_gc_duration_seconds_sum 0.006412249
go_gc_duration_seconds_count 7
# HELP go_goroutines Number of goroutines that currently exist.
# TYPE go_goroutines gauge
...
```

*  また Node exporter コンテナからも同様にメトリクスを取得できる.

```bash
$ curl http://localhost:9100/metrics

# HELP go_gc_duration_seconds A summary of the pause duration of garbage collection cycles.
# TYPE go_gc_duration_seconds summary
go_gc_duration_seconds{quantile="0"} 1.6083e-05
go_gc_duration_seconds{quantile="0.25"} 0.000199083
go_gc_duration_seconds{quantile="0.5"} 0.000445916
go_gc_duration_seconds{quantile="0.75"} 0.001235125
go_gc_duration_seconds{quantile="1"} 0.004044458
go_gc_duration_seconds_sum 0.031675077
go_gc_duration_seconds_count 39
# HELP go_goroutines Number of goroutines that currently exist.
# TYPE go_goroutines gauge
...
```

*  <http://localhost:9090/targets> にアクセスしてみると、Prometheus が監視している対象が一覧表示される.

![Targets](./images/multiple-targets.png)

*  <http://localhost:9090/graph> にアクセスしてみると、クエリ言語である PromQL を使うことで監視しているコンテナサーバのメトリクス情報を取得することができる.
    -  `up` や `process_resident_memory_bytes`、 `prometheus_tsdb_head_samples_appended_total` などの PromQL 式が使用できる.

![Charts](./images/multiple-charts.png)

##  References

*  <https://christina04.hatenablog.com/entry/prometheus-node-exporter>

